package com.tt.concurrent.callable;

import java.util.concurrent.Callable;

public class ComplexMathCallable {
    ComplexMath complexMath;
    public PartialCalculation[] partialCalculations = new PartialCalculation[]{
            () -> callable1(),
            () -> callable2(),
            () -> callable3(),
            () -> callable4(),
    };

    ComplexMathCallable() {
        this.complexMath = new ComplexMath(8000, 8000);
    }

    public Callable<Double> callable1() {
        return () -> complexMath.calculate(1, 4);
    }

    public Callable<Double> callable2() {
        return () -> complexMath.calculate(2, 4);
    }

    public Callable<Double> callable3() {
        return () -> complexMath.calculate(3, 4);
    }

    public Callable<Double> callable4() {
        return () -> complexMath.calculate(4, 4);
    }

    interface PartialCalculation {
        Callable<Double> calculate();
    }
}
