package com.tt.concurrent.callable;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * 1. Stwórz pulę wątków
 * a) Wykorzystaj Runtime.getRuntime().availableProcessors() aby pobrać ilość dostępnych procesów
 * b) pulę stwórz wykorzystując Executors.newFixedThreadPool(numberOfThreads);
 * 2. Stwórz zadania ComplexMathCallable w zależności od ilości wątków
 * 3. Zgłoś zadania do wykonania (metoda submit);
 * 4. Odłóż zwracane obiekty typu Future na listę - przydadzą się przy pobieraniu wyników
 * 5. Przeiteruj po liście i wywołaj metodę get() na obiektach Future
 * a) pamiętaj, że poszczególne wyniki należy zsumować
 * 6. Pamiętaj o zamknięciu ExecutorService'u - executor.shutdown();
 * 7. Zaprezentuj wyniki i zinterpretuj czas wykonania
 * <p>
 * WNIOSKI:
 * Czas wykonania programu zmalał w przybliżeniu 4 razy - z ok. 2 sekund do ok. 0,5 sekundy. Taki a nie inny wynik został spowodowany
 * podzieleniem zadania (metody calculate()) na maksymalnie 4 niezależne wątki.
 */
public class ComplexMathRunner {
    public static void main(String[] args) throws ExecutionException, InterruptedException {

        System.out.println("Program start...");

        int availableThreads = Runtime.getRuntime().availableProcessors();
        ExecutorService executorService = Executors.newFixedThreadPool(availableThreads);

        ComplexMathCallable complexMathCallable = new ComplexMathCallable();
        List<Future<Double>> futureList = new LinkedList<>();
        double finalResult = 0;

        long startTime = System.currentTimeMillis();

        for (int i = 0; i < complexMathCallable.partialCalculations.length; i++) {
            futureList.add(executorService.submit(complexMathCallable.partialCalculations[i].calculate()));
        }

        for (Future<Double> f : futureList) {
            finalResult += f.get();
        }

        long endTime = System.currentTimeMillis();
        System.out.println("Execution time: " + ((endTime - startTime) / 1000d) + " seconds.");

        executorService.shutdown();

        System.out.println("Final result: " + finalResult);

        System.out.println("\nCONCLUSION:\n" +
                "Program execution time decreased approximately 4 times - from about 2 seconds to about 0.5 seconds. This result was caused by\n" +
                "division of the task (calculate() method in particular) to at most 4 independent threads.");
    }
}
